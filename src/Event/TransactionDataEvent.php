<?php declare(strict_types=1);

namespace Drupal\commerce_netbilling\Event;

use Drupal\commerce_payment\Entity\PaymentInterface;
use Symfony\Component\EventDispatcher\Event;

class TransactionDataEvent extends Event {

  /**
   * Payment.
   *
   * @var \Drupal\commerce_payment\Entity\PaymentInterface
   */
  protected $payment;

  /**
   * Transaction data.
   *
   * @var array
   */
  protected $transactionData;

  /**
   * @inheritDoc
   */
  public function __construct(PaymentInterface $payment, array $transactionData) {
    $this->payment = $payment;
    $this->transactionData = $transactionData;
  }

  /**
   * Getter for payment.
   *
   * @return \Drupal\commerce_payment\Entity\PaymentInterface
   */
  public function getPayment(): PaymentInterface {
    return $this->payment;
  }

  /**
   * Getter for transaction data.
   *
   * @return array
   */
  public function getTransactionData(): array {
    return $this->transactionData;
  }

  /**
   * Setter for transaction data.
   *
   * @param array $transactionData
   */
  public function setTransactionData(array $transactionData): void {
    $this->transactionData = $transactionData;
  }

}
