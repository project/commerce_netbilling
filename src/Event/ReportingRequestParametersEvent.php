<?php declare(strict_types=1);

namespace Drupal\commerce_netbilling\Event;

use Symfony\Component\EventDispatcher\Event;

class ReportingRequestParametersEvent extends Event {

  /**
   * Array of request parameters.
   *
   * @var array
   */
  protected $parameters;

  /**
   * Context.
   *
   * @var array
   */
  protected $context;

  /**
   * @inheritDoc
   */
  public function __construct(array $parameters, array $context = []) {
    $this->parameters = $parameters;
    $this->context = $context;
  }

  /**
   * Get parameters.
   *
   * @return array
   */
  public function getParameters(): array {
    return $this->parameters;
  }

  /**
   * Set parameters.
   *
   * @param array $parameters
   */
  public function setParameters(array $parameters): void {
    $this->parameters = $parameters;
  }

  /**
   * Get context.
   *
   * @return array
   */
  public function getContext(): array {
    return $this->context;
  }

}
