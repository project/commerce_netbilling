<?php declare(strict_types=1);

namespace Drupal\commerce_netbilling\Event;

/**
 * Commerce NETbilling events.
 */
final class CommerceNetbillingEvents {

  /**
   * Transaction data collection event.
   */
  const TRANSACTION_DATA = 'commerce_netbilling.transaction_data';

  /**
   * Reporting request parameters.
   */
  const REPORTING_REQUEST_PARAMS = 'commerce_netbilling.reporting_request_params';

}
