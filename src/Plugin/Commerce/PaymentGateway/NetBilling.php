<?php declare(strict_types=1);

namespace Drupal\commerce_netbilling\Plugin\Commerce\PaymentGateway;

use CommerceGuys\Intl\Formatter\NumberFormatterInterface;
use DateTime;
use Drupal\commerce_netbilling\Event\CommerceNetbillingEvents;
use Drupal\commerce_netbilling\Event\ReportingRequestParametersEvent;
use Drupal\commerce_netbilling\Event\TransactionDataEvent;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\CreditCard;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PaymentMethodStorageInterface;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsCreatingPaymentMethodsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsStoredPaymentMethodsInterface;
use Drupal\commerce_payment\Annotation\CommercePaymentGateway;
use Drupal\commerce_price\Price;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides the NETbilling payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "netbilling",
 *   label = "NETbilling",
 *   display_label = "NETbilling",
 *   payment_method_types = {"credit_card"},
 *   modes = {
 *     "live" = @Translation("Live"),
 *   },
 *   requires_billing_information = FALSE,
 * )
 */
class NetBilling extends OffsitePaymentGatewayBase implements SupportsStoredPaymentMethodsInterface, SupportsCreatingPaymentMethodsInterface {

  /**
   * Endpoint.
   */
  const ENDPOINT = 'https://secure.netbilling.com:1402/gw/sas/';

  /**
   * The timestamp acceptable to the member/transaction reporting endpoint.
   */
  const REPORTING_TIME_FORMAT = 'Y-m-d H:i:s';

  /**
   * Number formatter.
   *
   * @var \CommerceGuys\Intl\Formatter\NumberFormatterInterface
   */
  protected $numberFormatter;

  /**
   * Event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $loggerChannel;

  /**
   * {@inheritDoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    PaymentTypeManager $payment_type_manager,
    PaymentMethodTypeManager $payment_method_type_manager,
    TimeInterface $time,
    NumberFormatterInterface $numberFormatter,
    EventDispatcherInterface $eventDispatcher,
    DateFormatterInterface $dateFormatter,
    LoggerInterface $loggerChannel
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $entity_type_manager,
      $payment_type_manager,
      $payment_method_type_manager,
      $time
    );
    $this->numberFormatter = $numberFormatter;
    $this->eventDispatcher = $eventDispatcher;
    $this->dateFormatter = $dateFormatter;
    $this->loggerChannel = $loggerChannel;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('commerce_price.number_formatter'),
      $container->get('event_dispatcher'),
      $container->get('date.formatter'),
      $container->get('logger.channel.commerce_payment')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration() {
    return [
      'account_id' => '',
      'dynip_sec_code' => '',
      'retrieval_keyword' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['mode']['#disabled'] = TRUE;
    $form['mode']['#default_value'] = 'live';
    $form['mode']['#description'] = $this->t('Live/Test mode is toggled on the account.');
    $form['account_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Account ID'),
      '#default_value' => $this->configuration['account_id'],
      '#required' => TRUE,
    ];
    $form['dynip_sec_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Dynamic IP security code'),
      '#description' => $this->t('Dynamic IP access security code. This is optional at the gateway level but IP source security alone is not advised.'),
      '#default_value' => $this->configuration['dynip_sec_code'],
      '#required' => TRUE,
    ];
    $form['retrieval_keyword'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Data retrieval keyword'),
      '#description' => $this->t('Retrieval keywords are scoped per NETbilling "site key." If you must alter it dynamically, alter the request programmatically.'),
      '#default_value' => $this->configuration['retrieval_keyword'],
      '#required' => TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['account_id'] = $values['account_id'];
      $this->configuration['dynip_sec_code'] = $values['dynip_sec_code'];
      $this->configuration['retrieval_keyword'] = $values['retrieval_keyword'];
    }
  }

  /**
   * Get the request client.
   *
   * @return \GuzzleHttp\Client
   */
  protected function getClient(): Client {
    return new Client([
      'base_uri' => self::ENDPOINT,
    ]);
  }

  /**
   * Get base POST params.
   *
   * @return string[]
   */
  protected function basePostParams(): array {
    return [
      'account_id' => $this->configuration['account_id'],
      'dynip_sec_code' => $this->configuration['dynip_sec_code'],
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    // Remote IDs for NETbilling "PCI storage" repeat billing are based on the
    // initial transaction ID where the user provided the card data.
    // @see https://secure.netbilling.com/public/docs/merchant/public/directmode/directmode3protocol.html
    // Card tokens are in the form of CS:123456789012 for the card_number,
    // where the numeric component is the original trans_id.
    assert($payment->getAmount()->getCurrencyCode() === 'USD');
    $lineItemLabels = [];
    foreach ($payment->getOrder()->getItems() as $item) {
      $lineItemLabels[] = sprintf('%s: %s', $item->label(), $item->getTotalPrice());
    }
    $postData = [
      'account_number' => $payment->getPaymentMethod()->getRemoteId(),
      'pay_type' => 'C',
      'tran_type' => $capture ? 'S' : 'A',
      // Description otherwise inherits from the original transaction.
      'description' => implode("\n", $lineItemLabels),
      'user_data' => implode("\n", [
        'Order-ID: ' . $payment->getOrderId(),
        'Customer-ID: ' . $payment->getPaymentMethod()->getOwnerId(),
      ]),
      // "No spaces, commas or currency signs."
      'amount' => $this->numberFormatter->format(
        $payment->getAmount()->getNumber(),
        [
          'use_grouping' => FALSE,
          'minimum_fraction_digits' => 2,
          'maximum_fraction_digits' => 2,
        ]
      ),
    ] + $this->basePostParams();
    $event = new TransactionDataEvent($payment, $postData);
    $this->eventDispatcher->dispatch($event, CommerceNetbillingEvents::TRANSACTION_DATA);
    try {
      $response = $this->getClient()->post('direct3.2', [
        // Default encoding is PHP_QUERY_RFC1738
        RequestOptions::BODY => http_build_query($event->getTransactionData()),
        RequestOptions::HEADERS => [
          'content-type' => 'application/x-www-form-urlencoded',
        ],
        RequestOptions::HTTP_ERRORS => TRUE,
      ]);
    }
    // Can't typehint specifically to RequestException because NETbilling sends
    // 6xx codes and other non-conformant fun.
    catch (\Throwable $e) {
      throw new PaymentGatewayException($e->getMessage(), $e->getCode(), $e);
    }
    $data = [];
    parse_str($response->getBody()->getContents(), $data);
    $payment->setRemoteState($data['status_code']);
    if (in_array($payment->getRemoteState(), ['0', 'F'])) {
      throw new PaymentGatewayException('Gateway error.');
    }
    $payment->setRemoteId($data['trans_id']);
    $payment->setState($capture ? 'completed' : 'authorization');
    $payment->save();
  }

  /**
   * {@inheritDoc}
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    // @todo Implement soft delete.
    throw new PaymentGatewayException('Deleting payment methods at NETbilling not supported.');
  }

  /**
   * {@inheritdoc}
   */
  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details) {
    $transactionId = $payment_details['Ecom_Ezic_Response_TransactionID'];
    $payment_method->setRemoteId('CS:' . $transactionId);
    // NETbilling does not send back meaningful information about the card.
    // Request info.
    $transactionDate = \DateTime::createFromFormat(
      self::REPORTING_TIME_FORMAT,
      $payment_details['Ecom_Ezic_Response_IssueDate'],
      new \DateTimeZone('UTC')
    );
    $transactionData = $this->reportingRequest(
      (int) $transactionDate->format('U'),
      (int) $transactionDate->format('U') + 1
    );
    $transaction = $transactionData[$transactionId];
    $payment_method->card_type = strtolower($transaction['CARD_TYPE']);
    $payment_method->card_number = substr($transaction['CARD_NUMBER'], strlen($transaction['CARD_NUMBER']) - 4);
    // It's a little unclear how expiration date updating happens at NETbilling,
    // but we will store what we have, here.
    $payment_method->card_exp_month = substr($transaction['CARD_EXPIRE'], 0, 2);
    $payment_method->card_exp_year = (DateTime::createFromFormat('y', substr($transaction['CARD_EXPIRE'], 2, 2)))
      ->format('Y');
    $expires = CreditCard::calculateExpirationTimestamp($payment_method->card_exp_month->value, $payment_method->card_exp_year->value);
    $payment_method->setExpiresTime($expires);
    $payment_method->save();
  }

  /**
   * {@inheritDoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $payment_method_storage = $this->entityTypeManager->getStorage('commerce_payment_method');
    assert($payment_method_storage instanceof PaymentMethodStorageInterface);
    $payment_method = $payment_method_storage->createForCustomer(
      'credit_card',
      $this->parentEntity->id(),
      $order->getCustomerId(),
      $order->getBillingProfile()
    );
    // The payment method is created first so that it can be attached to the
    // generated payment transaction.
    $data = $request->query->all();
    $this->createPaymentMethod($payment_method, $data);
    $this->createPaymentFromReturnData($data, $payment_method, $order);
  }

  /**
   * Create a payment from return data.
   *
   * This method is abstracted out to support conditions under which payment
   * methods or payments are initiated via non-HTML based checkout, e.g.
   * decoupled front-ends with json:api.
   *
   * @param array $data
   *   Data extracted from the query string.
   * @param \Drupal\commerce_payment\Entity\PaymentMethodInterface $paymentMethod
   *   Payment Method.
   * @param \Drupal\commerce_order\Entity\OrderInterface|null $order
   *   Order. Optional.
   *
   * @return \Drupal\commerce_payment\Entity\PaymentInterface
   *   The saved payment.
   */
  public function createPaymentFromReturnData(array $data, PaymentMethodInterface $paymentMethod, ?OrderInterface $order = NULL): PaymentInterface {
    assert(!empty($data['Ecom_Ezic_Response_IssueDate']) && !empty($data['Ecom_Ezic_Response_StatusCode']) && !empty($data['Ecom_Cost_Total']) && !empty($data['Ecom_Ezic_Response_TransactionID']));
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    $transactionDate = \DateTime::createFromFormat(
      self::REPORTING_TIME_FORMAT,
      $data['Ecom_Ezic_Response_IssueDate'],
      new \DateTimeZone('UTC')
    );
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $payment_storage->create(array_filter([
      // This assumes only successful transactions are posted back...
      'state' => intval($data['Ecom_Ezic_Response_StatusCode']) === 1
        ? 'completed'
        : 'authorization',
      // Currency is not sent; NETbilling only initiates USD...
      'amount' => new Price((string) $data['Ecom_Cost_Total'], 'USD'),
      'payment_gateway' => $this->parentEntity->id(),
      'order_id' => $order ? $order->id(): NULL,
      'remote_id' => $data['Ecom_Ezic_Response_TransactionID'],
      'remote_state' => (string) $data['Ecom_Ezic_Response_StatusCode'],
      'payment_method' => $paymentMethod,
    ]));
    if ($payment->getRemoteState() == '1') {
      $payment->setCompletedTime($transactionDate->format('U'));
    }
    $payment->save();
    return $payment;
  }

  /**
   * Format a reporting-endpoint acceptable date.
   *
   * @param string $timestamp
   *   UNIX timestamp, converted in UTC
   * @return string
   *   The formatted date.
   */
  protected function formatReportingDate($timestamp) {
    return $this->dateFormatter->format($timestamp, 'custom', self::REPORTING_TIME_FORMAT, 'UTC');
  }

  /**
   * Make a request to the membership or transactions reporting endpoint.
   *
   * @param int|NULL $from
   *   Unix timestamp for from date; defaults to now.
   * @param int|NULL $to
   *   Unix timestamp for to date; optional
   * @param array $context
   *   Array of contextual data passed to the event dispatcher.
   *
   * @returns array
   *   Array of results.
   */
  public function reportingRequest(?int $from = NULL, ?int $to = NULL, $context = []) {
    // We must at the very least specify a "from" date
    $params['transactions_after'] = $this->formatReportingDate($from ?: time());
    if ($to) {
      $params['transactions_before'] = $this->formatReportingDate($to);
    }

    return $this->sendReportingRequest($params, $context);
  }

  /**
   * Make a request to the transaction reporting endpoint.
   *
   * @see http://secure.netbilling.com/public/docs/merchant/public/directmode/repinterface1.5.html
   *
   * @param array $params
   *   Array of parameters to build into the request.
   * @param array $context
   *   Context for event.
   *
   * @throws \Exception
   *
   * @returns mixed
   *   Array of array containing rows, and column headers (as keys => index), or FALSE on failure
   */
  protected function sendReportingRequest(array $params, array $context = []): array {
    $localParams = [
      'account_id' => $this->configuration['account_id'],
      'authorization' => $this->configuration['retrieval_keyword'],
    ];
    $params = array_filter($localParams) + $params;
    $options = [
      RequestOptions::HEADERS => [
        'User-Agent' => 'Drupal/Version:2020.Jul.04',
      ],
      RequestOptions::BODY => '',
    ];
    $event = new ReportingRequestParametersEvent($params, $context);
    $this->eventDispatcher
      ->dispatch($event, CommerceNetbillingEvents::REPORTING_REQUEST_PARAMS);
    foreach ($event->getParameters() as $field => $value) {
      if (is_array($value)) {
        foreach ($value as $v) {
          $options[RequestOptions::BODY] .= http_build_query([$field => $v]) . '&';
        }
      }
      else {
        $options[RequestOptions::BODY] .= http_build_query([$field => $value]) . '&';
      }
    }
    $options['body'] = trim($options[RequestOptions::BODY], " \t\n\r\0\x0B\\&");
    try {
      $client = new Client();
      $result = $client->post(
        'https://secure.netbilling.com/gw/reports/transaction1.5',
        $options
      );
      // Errors could also manifest in different response codes/headers.
      $contentType = $result->getHeader('content-type');
      if (($contentType && (reset($contentType) === 'text/plain')) || ($retry = $result->getHeader('retry-after'))) {
        if (isset($retry)) {
          $msg = $result->getBody() . ' / ' . $this->stringTranslation->translate('Retry after :s seconds', [':s' => reset($retry)]);
          $code = 429; // Too Many Requests
          // @todo - Implement caching the response and enforcing Retry-After interval.
        }
        else {
          $body = $result->getBody()->getContents();
          [$code, $msg] = explode(' ', $body, 2);
        }
        // Errors are indicated by the content-type of the response - code is first part of the body.
        throw new \Exception($msg, (int) $code);
      }
    }
    catch (\Exception $e) {
      $this->loggerChannel->error($e->getMessage());
      throw $e;
    }

    return $this->parseReport($result);
  }

  /**
   * Parser for CSV-style responses from the retrieval interface.
   *
   * @param \Psr\Http\Message\ResponseInterface $result
   *   Response.
   *
   * @return array
   *   Array of transaction data.
   */
  protected function parseReport(ResponseInterface $result): array {
    $results = [];
    $keys = [];
    $content = $result->getBody()->getContents();
    $csv = array_map('str_getcsv', explode("\n", trim($content)));
    foreach ($csv as $row => $line) {
      if ($row === 0) {
        $keys = $line;
      }
      else {
        $trans = array_combine($keys, $line);
        $results[$trans['TRANS_ID']] = $trans;
      }
    }
    return $results;
  }

}
